
# SET GPIO 23 HIGH TO DISABLE THE LED/FLASH on boot up

#pulse GPIO line to RPI 
import RPi.GPIO as GPIO

#turning off warning of GPIO already in use
GPIO.setwarnings(False)

#Set as Broadcom GPIO numbers
GPIO.setmode(GPIO.BCM)

#Pin for LED Flash
FLASH_PIN = 23

#set pin as output
GPIO.setup(FLASH_PIN, GPIO.OUT)
#set pin HI to disable flash on boot up
GPIO.output(FLASH_PIN, GPIO.LOW)
