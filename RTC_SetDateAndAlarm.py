"""RTC_SetDateAndAlarm.py

Introduction: 

The purpose of this script is communicate with the Real Time Clock (RTC) via I2C protocol.
The functions will turn off the RTC alarm, set the correct date and time on the RTC, and set an 
alarm for the desired time of day. 

The code is setup to default to 6 AM and 6 PM daily (12 hours).  With TestMode enabled, the alarm will
go off every 3 minutes.

"""

#========================================================================
#                            MODES                                
#========================================================================

#Test Mode: set TRUE for 3-minute alarm, FALSE for 12-hour alarm
testMode = True

#Verbose: Set TRUE to print clock and alarm registers from RTC in 1-second increments
#Note: Keep FALSE whenever possible, as RPi won't shutdown if TRUE
VERBOSE = False 

#========================================================================
#                            MODULE DEFINES                                
#========================================================================

#I2C Setup
import smbus

#Setup for sleep zzzzz
import time

#Date and Time Setup
from datetime import datetime

#I2C channel 1 is connected to GPIO pins

channel = 1

# Rx8900 read and write address

address = 0x32

#RX8900 Registers:

SEC_reg = 0x00      #Second
MIN_reg = 0x01      #Minute
HOUR_reg = 0x02     #Hour
WEEK_reg = 0x03     #Week
DAY_reg = 0x04      #Day
MONTH_reg = 0x05    #Month
YEAR_reg = 0x06     #Year

MIN_alarm_reg = 0x08    #Alarm Minute
HOUR_alarm_reg = 0x09   #Alarm Hour
WEEK_DAY_Alarm_reg = 0x0A #Alar Day of week

Extension_Register_reg = 0x0D

#Register of Interrupt Flags, includes Alarm Flag
Flag_Register_reg = 0x0E 

Control_Register_reg = 0x0F

#Alarm value for daily use
DAILY = 1<<7 

#Alarm Enable Mask
ALARM_ENABLE = 0b00001000

#Reset Enable Mask
RESET_ENABLE = 0b00000001

#========================================================================
#                            FUNCTIONS                              
#========================================================================


#**************************************************************************/
#Function: getWeekdayVal

#  @brief Sets the day of the week for register based on input value
#  @param inputVal: Sunday = 0, Monday = 1, Tuesday = 2, etc.
#  @return WeekdayVal: 8-bit value corresponding to day of the week
#**************************************************************************/
def getWeekdayVal(inputVal):
    WeekdayVal = 1<<int(inputVal)
    return WeekdayVal

#**************************************************************************/
#Function: dec2BCD

#  @brief Converts decimal to BCD (Binary Coded Decimal) format used by RTC
#  @param input val: decimal value of day, hour, minute, second etc. 
#  @return BCDOut: 8-bit value encoded in BCD format
#**************************************************************************/
def dec2BCD(inputValue): 
    x = str(inputValue)
    BCDOut = 0
    for char in x:
        BCDOut = (BCDOut << 4) + int(char)
    return BCDOut

#**************************************************************************/
#Function: initializeRTC

#  @brief Sets up RTC for desired use, including alarm enable and alarm clearing
#**************************************************************************/
def initializeRTC():
    # Note: ensure FOE pin is grounded to disable FOUT
    extensionRegWrite = 0x48 # example code sets week alarm, 1 Hz to Fout
    bus.write_byte_data(address, Extension_Register_reg, extensionRegWrite)

    flagRegWrite = 0x00 # Clear flag register (turn off alarm)
    bus.write_byte_data(address, Flag_Register_reg, flagRegWrite)

    controlReg = bus.read_byte_data(address, Control_Register_reg) #Read in current control register
    writeControl = controlReg | ALARM_ENABLE | RESET_ENABLE # enable alarm and reset (resets clock counter values)
    bus.write_byte_data(address, Control_Register_reg, writeControl) #Write out control register
    return

#**************************************************************************/
#Function: setDateTime

#  @brief Gets the current date and time and sets values on RTC
#**************************************************************************/
def setDateTime():
    #Writes current date and time values to RTC
    bus.write_byte_data(address, YEAR_reg, currYear)
    bus.write_byte_data(address, MONTH_reg, currMonth)
    bus.write_byte_data(address, DAY_reg, currDay)
    bus.write_byte_data(address, WEEK_reg, currWeekDay)
    bus.write_byte_data(address, HOUR_reg, currHour)
    bus.write_byte_data(address, MIN_reg, currMinute)
    bus.write_byte_data(address, SEC_reg, currSecond)
    return

#**************************************************************************/
#Function: setAlarm

#  @brief Sets alarm on RTCfor desired times to wake up RTC.  Default is 6 AM
#       and 6 PM. With testMode enabled, will set alarm for 3 minutes after current time.
#**************************************************************************/
def setAlarm():

    #Sets alarm to daily
    alarmWeekDay = DAILY 
    bus.write_byte_data(address, WEEK_DAY_Alarm_reg, alarmWeekDay) 

    if (testMode): #If test mode enabled, sets alarm minute to 3 min. after current min.
        

        currHour_int = int(now.strftime("%H"))      #grab hour int from RPi
        currMinute_int = int(now.strftime("%M"))    #grab minute int from RPi

        
        if currMinute_int < 57: #Check for 60-minute overflow
            testAlarmMinute = dec2BCD(currMinute_int + 3) #sets alarm minute to 3 min. after current min.
            testAlarmHour = dec2BCD(currHour_int) #Sets alarm hour to current hour
        else: #if overflow to next hour
            testAlarmMinute = dec2BCD(3 - (60-currMinute_int)) #Set to x min. after hour
            testAlarmHour = dec2BCD(currHour_int + 1)


        bus.write_byte_data(address, HOUR_alarm_reg, testAlarmHour)
        bus.write_byte_data(address, MIN_alarm_reg, testAlarmMinute)
    else:

        #Sets alarm minute to 0
        alarmMinute = dec2BCD(0) 
        #Sets alarm hour to 6 AM
        intMorningAlarmHour = 6
        morningAlarmHour = dec2BCD(intMorningAlarmHour)
        #Sets Evening alarm to 12 hours later, 6 PM
        eveningAlarmHour = dec2BCD(intMorningAlarmHour + 12) # 6PM

        #24-hour threshold for morning vs evening
        eveningAlarmTimeThresh = 17 

        #set alarm minute to be exactly at the changing of the hour
        bus.write_byte_data(address, MIN_alarm_reg, alarmMinute)
        # if its after 5pm (1 hr threshold) using set morning alarm
        if (bus.read_byte_data(address, HOUR_reg) > dec2BCD(eveningAlarmTimeThresh)):
            # set morning alarm
            bus.write_byte_data(address, HOUR_alarm_reg, morningAlarmHour)
            print("alarm set morning")
        else:
            # if its before 5 pm (IE, morning alarm went off, set evening alarm)
            bus.write_byte_data(address, HOUR_alarm_reg, eveningAlarmHour)
            print("alarm set evening")
    return



#========================================================================
#                            MAIN CODE                                
#========================================================================

#initialize I2C
bus = smbus.SMBus(channel)


#Getting current date & time
now = datetime.now()

# Get time and date information from RPi
currYear = dec2BCD(int(now.strftime("%Y"))-2000) # value between 00 and 99 (years 2000 - 2099)
currMonth = dec2BCD(now.strftime("%m")) # jan = 1, feb = 2, mar = 3, etc etc
currDay = dec2BCD(now.strftime("%d")) # values 0-31
currWeekDay = dec2BCD(getWeekdayVal(now.strftime("%w"))) # sun = 1, mon = 2, tue = 3, etc etc
currHour = dec2BCD(now.strftime("%H")) # values 0-23
currMinute = dec2BCD(now.strftime("%M")) # values 0-59
currSecond = dec2BCD(now.strftime("%S")) # values 0-59

#Call above functions to initialize and set time & alarm
initializeRTC()
setDateTime()
setAlarm()

#If Verbose mode enabled, will print registers every second
if(VERBOSE):
    #Continually prints
    while True:
        time.sleep(1) #1 second delay
        #Print status of registers
        print("seconds ", bus.read_byte_data(address, SEC_reg))
        print("min ",bus.read_byte_data(address,MIN_reg))
        print("hour ",bus.read_byte_data(address,HOUR_reg))
        print("flag register ", bus.read_byte_data(address, Flag_Register_reg))


#Call Shutdown script to begin shutdown process
#Note: only neccessary due to issues with RPi shutdown sequence 
#   allowing for only a single script to be called.

import PIC_Shutdown_Warning_Pulse as SHTDN 

SHTDN.runScript()

"""PIC_Shutdown_Warming_Pulse.py

The purpose of this script is to send a falling edge to the PIC before shutdown.
Such a signal sets a 15 s timer on the PIC to turn off power completely, giving
RPi time to safely shut off.


#pulse GPIO line to RPI
import RPi.GPIO as GPIO
# import time for sleep zzzzzz
#import time

#Set as Broadcom GPIO numbers
GPIO.setmode(GPIO.BCM)

#GPIO Pin to Pulse
PULSE_PIN = 18

#set pin as output
GPIO.setup(PULSE_PIN, GPIO.OUT)

#pulse pin HI then LO
GPIO.output(PULSE_PIN, GPIO.HIGH)
time.sleep(1)
GPIO.output(PULSE_PIN, GPIO.LOW)

#Turns GPIO pins to Hi-impedence
GPIO.cleanup()
"""
