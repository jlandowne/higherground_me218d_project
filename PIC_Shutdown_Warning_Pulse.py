"""PIC_Shutdown_Warming_Pulse.py

The purpose of this script is to send a falling edge to the PIC before shutdown.
Such a signal sets a 15 s timer on the PIC to turn off power completely, giving
RPi time to safely shut off. Note: Function needs to be called by another script
due to issues with shutdown sequence on RPi.
"""

#**************************************************************************/
#Function: runScript:

#  @brief Runs the script to pulse the shutdown line
#**************************************************************************/
def runScript(): 
    #pulse GPIO line to RPI
    import RPi.GPIO as GPIO
    # import time for sleep zzzzzz
    import time

    #Set as Broadcom GPIO numbers
    GPIO.setmode(GPIO.BCM)

    #GPIO Pin to Pulse
    PULSE_PIN = 18

    #set pin as output
    GPIO.setup(PULSE_PIN, GPIO.OUT)

    #pulse pin HI then LO
    GPIO.output(PULSE_PIN, GPIO.HIGH)
    time.sleep(1)
    GPIO.output(PULSE_PIN, GPIO.LOW)

    #Turns GPIO pins to Hi-impedence
    GPIO.cleanup()
    return