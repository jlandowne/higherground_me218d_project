/*!
 * @file main.c
 *
 * @section intro_sec Introduction
 *
 * This is the main file for the PIC12F629.  This microcontroller controls the power to
 * RPI via the High Side Switch (HSS) and RTC.  The RTC alarm will trigger an interrupt 
 * to wake the PIC up from sleep, after which, it will turn on the HSS. A shutdown signal
 * from the RPI will trigger an interupt to set a ~15 s timer to turn off power to the HSS
 * and put the PIC into low-power sleep.
 *
 * @section author Author
 *
 * Written by Justin Landowne and William Walecka for ME218D and Higher Ground
 * Dated 11/2/19
/**************************************************************************/

/*========================================================================*/
/*                            INCLUDED FILES                              */
/*========================================================================*/
#include <stdio.h>          //C library to perform Input/Output operations
#include <stdlib.h>         //C Standard General Utilities Library
#include <assert.h>         //C Diagnostics Library  
#include <xc.h>             //Standard PIC compiler library
#include <pic12f629.h>      //Standard library for PIC12F752

/*========================================================================*/
/*                              DEFINES                                   */
/*========================================================================*/

//Timer register starting values
//  Note: Timer registers will start from 0 and overflow x times
#define TIMER1LOWER 0x00 
#define TIMER1UPPER 0x00

//Number of times for timer to overflow for overall time count before
//shutdown signal to HSS
//  Timer values 58125 in registers = 1 minute (pre-scaler = 8, FOSC = 31KHZ)
//  Calculation: 60s = (4 [instructions per cycle]*val)/(FOSC/8 [prescaler]), solve for "val"
#define TIMER_COUNT 35 // 140 overflows = 60sec., 35 overflows = 15 sec.

//Macros for turning on and off HSS (Power to RPi)
#define HSS_ON() (GPIObits.GP0 = 1) // turn high side switch on
#define HSS_OFF() (GPIObits.GP0 = 0) // turn high side switch off

//Configuration settings of PIC
    //WDTE = Watchdog timer enable
    //BOREN = Brownout reset enable
    //PWRTE = Power up timer enable
    //MCLRE = Master clear enable
    //FOSC = Oscillator Selection
    //  INTRCIO = Internal RC oscillator, no clock out
//__CONFIG(_WDT_OFF & _PWRT_OFF & _MCLRE_ON & _BOREN_ON & _CP_OFF & _CPD_OFF & _INTRC_OSC_NOCLKOUT);
#pragma config WDTE = OFF, BOREN = OFF, PWRTE = OFF, MCLRE = ON, FOSC=INTRCIO

/*========================================================================*/
/*                        FUNCTION PROTOTYPES                             */
/*========================================================================*/
static void initialize(void);
static void go2Sleep(void);


/*========================================================================*/
/*                           MAIN CODE                                    */
/*========================================================================*/
void main(void) {

    initialize(); // initialize IO pins and peripherals
    
   
    while (1)
    {
        // run fovever
        // everything happens in myISR
    }
}


/*========================================================================*/
/*                        Private Functions                               */
/*========================================================================*/

/**************************************************************************/
/*!Function: Initialize

  @brief Initializes IO pins for interrupts from RTC and RPi.  Sets up timer
  for RPi shutdown sequence.  Sets up interrupts from RTC, RPi, and timer.

*/
/**************************************************************************/
static void initialize(void)
{
    //set HSS GPIO - pin 0 (output)
    //set interrupt from RTC - pin 1 (input)
    //set interrupt from RPi - pin 2 (input)
    //set pins 4 as unused (input)
    
    TRISIObits.TRISIO0 = 0;
    TRISIObits.TRISIO1 = 1;
    TRISIObits.TRISIO2 = 1;
    TRISIObits.TRISIO4 = 1;
    TRISIObits.TRISIO5 = 1;
    
    //Sets comparator off (to enable digitial input)
    CMCONbits.CM0= 1;
    CMCONbits.CM1= 1;
    CMCONbits.CM2= 1;
    
    // turn on HSS on powerup of PIC
    HSS_ON(); 
    
    //enable global interrupts 
    INTCONbits.GIE = 1;
    //enable peripheral interrupts 
    INTCONbits.PEIE = 1;
    //Enable GPIO interrupts
    INTCONbits.GPIE = 1;
    //enable interrupt on change
    IOCbits.IOC1 = 1;
    //Interrupt on falling edge interrupts for GP2
    OPTION_REGbits.INTEDG = 0; //Falling edge
    INTCONbits.INTE = 1; //Enable GP2 int
    
    //enable internal pull ups
    OPTION_REGbits.nGPPU = 0;
    WPUbits.WPU1 = 1;
    WPUbits.WPU2 = 1;
    
    //Enable Timer 1
    //Timer 1 interrupt
    PIE1bits.TMR1IE = 1;
    
    //set pre-scaler as 8
    T1CONbits.T1CKPS0 = 1;
    T1CONbits.T1CKPS1 = 1;
    //set Timer 1 register starting value
    TMR1H = TIMER1UPPER;
    TMR1L = TIMER1LOWER;        
    
}

/**************************************************************************/
/*!Function: turnOnTimer1

  @brief Starts Timer 1 counting
*/
/**************************************************************************/
void turnOnTimer1(void)
{
    T1CONbits.TMR1ON = 1;
}

/**************************************************************************/
/*!Function: timerCounter

  @brief Keeps track of Timer 1 counter overflows for shutdown sequence.
  Upon reaching max timer overflows, initiates HSS off and PIC sleep.
*/
/**************************************************************************/
void timerCounter(void)
{
    static char count;
    //Increment count
    count++;
    //Reset timer registers (set to 0)
    TMR1H = TIMER1UPPER;
    TMR1L = TIMER1LOWER; 
    //If max overflows reached
    if (count == TIMER_COUNT)
    {
         T1CONbits.TMR1ON = 0; //Turn off Timer 1
        //reset count
        count = 0;
        //Go into low-power sleep mode on PIC
        go2Sleep();
    }
    
}

/**************************************************************************/
/*!Function: go2Sleep

  @brief Turns off power to RPi and puts PIC into low-power sleep mode to 
  later be awoken by RTC alarm interrupt.
*/
/**************************************************************************/
static void go2Sleep(void)
{
    HSS_OFF(); //Turn off HSS to turn off RPI
    SLEEP();
}

/**************************************************************************/
/*!Function: myISR

  @brief Interrupt Response Routine (ISR) that handles all interrupts
  from timer, RTC, and RPi.  Turns on HSS to power RPi when receives interrupt
  from RTC.  Starts shutdown timer when receives interrupt from RPi.  Increments
  timer overflow counter when receives Timer 1 overflow interrupt.
*/
/**************************************************************************/
void __interrupt() myISR()
{
            
    if (INTCONbits.GPIF) // if there was an interrupt on change (from RTC)
    {
        INTCONbits.GPIF = 0; // clear interrupt flag
        if (!GPIObits.GPIO1) // if RTC pulled line low to request wake up
        {
           HSS_ON(); // turn on power to RPi
        }
    }
    if (INTCONbits.INTF) // GP2 Interrupt (interrupt on falling edge only) from RPi
    {
        INTCONbits.INTF = 0; // clear interrupt flag

        if(!GPIObits.GPIO2) // if RPi pulled line low to request shutdown
        {
            turnOnTimer1(); // start shutdown timer to let RPi shut down
        }
    }
    //if Timer 1 overflows 
    else if(PIR1bits.TMR1IF)
    {
        //clear interrupt flag
        PIR1bits.TMR1IF = 0;
        //increment timer and check if max overflows reached
        timerCounter();
    }
}