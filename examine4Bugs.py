from __future__ import print_function
import cv2 as cv
from skimage.measure import compare_ssim
from skimage import io
import numpy as np
import argparse
import imutils

'''
image = cv.imread("clouds.jpg")
gray_image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
cv.imshow("Over the Clouds", image)
cv.imshow("Over the Clouds - gray", gray_image)
cv.waitKey(0)
cv.destroyAllWindows()
'''

desiredWidth = 1200
MAX_FEATURES = 500
GOOD_MATCH_PERCENT = 0.15

# area the bug grid takes up in the picture
# this makes sure any weird stuff outside the grid is ignored
CENTRAL_X = 100
CENTRAL_Y = 100
CENTRAL_W = 1000
CENTRAL_V = 800

def alignImages(im1, im2):
    
    # convert images to grayscale
    im1Gray = cv.cvtColor(im1, cv.COLOR_BGR2GRAY)
    im2Gray = cv.cvtColor(im2, cv.COLOR_BGR2GRAY)
    
    # detect ORB features and compute descriptors
    orb = cv.ORB_create(MAX_FEATURES)
    (keypoints1, descriptors1) = orb.detectAndCompute(im1Gray, None)
    (keypoints2, descriptors2) = orb.detectAndCompute(im2Gray, None)
    
    # match features
    matcher = cv.DescriptorMatcher_create(cv.DESCRIPTOR_MATCHER_BRUTEFORCE_HAMMING)
    matches = matcher.match(descriptors1, descriptors2, None)
    
    # sort matches by score
    matches.sort(key=lambda x: x.distance, reverse=False)
    
    # remove not so good matches
    numGoodMatches = int(len(matches) * GOOD_MATCH_PERCENT)
    matches = matches[:numGoodMatches]
    
    # draw top matches
    imMatches = cv.drawMatches(im1, keypoints1, im2, keypoints2, matches, None)
    cv.imwrite("matches.jpg", imMatches)
    
    # extract location of good matches
    points1 = (np.zeros((len(matches), 2), dtype=np.float32))
    points2 = (np.zeros((len(matches), 2), dtype=np.float32))
    
    for i, match in enumerate(matches):
        points1[i, :] = keypoints1[match.queryIdx].pt
        points2[i, :] = keypoints2[match.trainIdx].pt
        
    # Find homography
    (h, mask) = cv.findHomography(points1, points2, cv.RANSAC)
    
    # Use homography
    (height, width, channels) = im2.shape
    im1Reg = cv.warpPerspective(im1, h, (width, height))
    
    return (im1Reg, h)
    

ap = argparse.ArgumentParser()
ap.add_argument("-f", "--first", required=True,
                help="first input image")
ap.add_argument("-s", "--second", required=True,
                help="second")
args = vars(ap.parse_args())

# load the two input images
imageA = cv.imread(args["first"])
imageB = cv.imread(args["second"])
(imHeight, imWidth, x) = imageA.shape
scaleFactor = desiredWidth/imWidth
width = int(imageA.shape[1] * scaleFactor)
height = int(imageA.shape[0] * scaleFactor)
dim = (width, height)
imageA = cv.resize(imageA, dim)
imageB = cv.resize(imageB, dim)

# Skew the original image
(imageB, h) = alignImages(imageB, imageA)

# convert to greyscale
grayA = cv.cvtColor(imageA, cv.COLOR_BGR2GRAY)
grayB = cv.cvtColor(imageB, cv.COLOR_BGR2GRAY)

# compute the Structural Similarity Index between the 
# two images
# we use a HUGE sigma because the skewing process creates a lot of small errors
# We should tune this later once we have a real trap and know what we're dealing with

# todo: maybe blur the photo slightly so those edges aren't quite so problematic for the
# difference algorithm?
(score, diff) = compare_ssim(grayA, grayB, gaussian_weights=True, sigma=1.5, full=True)
diff = (diff * 255).astype("uint8")
print("SSIM: {}".format(score))

# trying different blur filters
# pretty good so we'll use it
KERNAL_SIZE = (5, 5)    
# cv.imshow("Diff orig", cv.resize(diff, dim))
cv.imwrite("diff_orig.jpg", diff)
grayA_delta = cv.blur(grayA, KERNAL_SIZE)
grayB_delta = cv.blur(grayB, KERNAL_SIZE)
(score, diff) = compare_ssim(grayA_delta, grayB_delta, gaussian_weights=True, sigma=1.5, full=True)
# cv.imshow("Diff blur", cv.resize(diff, dim))
cv.imwrite("Diffblur.jpg", diff)
diff = (diff * 255).astype("uint8")
'''
# not much better than blur and a lot slower
grayA_delta = cv.GaussianBlur(grayA, KERNAL_SIZE, 1.5)
grayB_delta = cv.GaussianBlur(grayB, KERNAL_SIZE, 1.5)
(score, diff2) = compare_ssim(grayA_delta, grayB_delta, gaussian_weights=True, sigma=1.5, full=True)
cv.imshow("Diff gauss", cv.resize(diff2, dim))

# terrible, makes everything worse
grayA_delta = cv.medianBlur(grayA, 5)
grayB_delta = cv.medianBlur(grayB, 5)
(score, diff2) = compare_ssim(grayA_delta, grayB_delta, gaussian_weights=True, sigma=1.5, full=True)
cv.imshow("Diff median", cv.resize(diff2, dim))
'''

# threshold the difference image, followed by finding contours
# to obtain the regions of the input images that differ
thresh = cv.threshold(diff, 0, 255,
                       cv.THRESH_BINARY_INV | cv.THRESH_OTSU)[1]
cnts = cv.findContours(thresh.copy(), cv.RETR_EXTERNAL,
                        cv.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)

# loop over the contours
for c in cnts:
    # compute bounding box
    (x, y, w, h) = cv.boundingRect(c)
    # draw any rectangles that are within our original bounding box
    withinBox = True
    if (x < CENTRAL_X or x > (CENTRAL_X + CENTRAL_W)):
        withinBox = False
    if (x+w < CENTRAL_X or x+w > (CENTRAL_X + CENTRAL_W)):
        withinBox = False
    if (y < CENTRAL_Y or y > (CENTRAL_Y + CENTRAL_V)):
        withinBox = False
    if (y+h < CENTRAL_Y or y+h > (CENTRAL_Y + CENTRAL_V)):
        withinBox = False        
    if withinBox:
        # Make a cropped version of the photo and find te dominant color
       # cropped = imageB[y+int(h/4):y+int(0.75*h), x+int(w/4):x+int(0.75*w)]
        #cv.imshow("cropped", cropped)
        # print("average")
        #print(cropped.mean(axis=0).mean(axis=0))        
        #hsv = cv.cvtColor(cropped, cv.COLOR_BGR2HSV)
        # hsv[:,:,2] = 150
        #cropped = cv.cvtColor(hsv, cv.COLOR_HSV2BGR)        
        #cropped = cropped.reshape(-1, 3)
        #pixels = np.float32(cropped)
        '''
        nColors = 5
        criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 200, .1)
        flags = cv.KMEANS_RANDOM_CENTERS
        
        _, labels, palette = cv.kmeans(pixels, nColors, None, criteria, 10, flags)
        _, counts = np.unique(labels, return_counts=True)
        
        dominant = palette[np.argmax(counts)]
        print("dominant")
        print(dominant)
        # Now draw the rectangle on the main picture
        cv.rectangle(imageA, (x, y), (x + w, y + h), (0, 0, 255), 2)
        cv.rectangle(imageB, (x, y), (x + w, y + h), (0, 0, 255), 2)
    '''
# show the images while resizing everything
# cv.imshow("Original", cv.resize(imageA, dim))
cv.imwrite("original.jpg", imageA)
# cv.imshow("Modified", cv.resize(imageB, dim))
cv.imwrite("modified.jpg", imageB)
# cv.imshow("Diff", cv.resize(diff, dim))
cv.imwrite("diff.jpg", diff)
# cv.imshow("Thresh", cv.resize(thresh, dim))
cv.imwrite("thresh.jpg", thresh)
# cv.waitKey(0)