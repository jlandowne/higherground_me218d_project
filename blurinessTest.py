# coding: utf-8

# In[4]:


# import the necessary packages
from imutils import paths
import argparse
import cv2
 
def variance_of_laplacian(image):
# compute the Laplacian of the image and then return the focus
# measure, which is simply the variance of the Laplacian
# use a Ksize of 5, seems to give the best results
    return cv2.Laplacian(image, cv2.CV_64F, ksize=5).var()
 
imageSize = 1000
kernal_size = 4
GAUSS_KERNAL_SIZE = (7, 7)    
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="path to input directory of images")
ap.add_argument("-t", "--threshold", type=float, default=5500.0, help="focus measures that fall below this value will be considered 'blurry'")
args = vars(ap.parse_args())

# loop over the input images

# load the image, convert it to grayscale, and compute the
# focus measure of the image using the Variance of Laplacian
# method
image = cv2.imread(args["image"])
# image = cv2.resize(image, imageSize)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# gray = cv2.resize(gray, (1000, 80))
# gray = cv2.convertScaleAbs(gray)
# gray = cv2.normalize(gray, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_64F)
gray2 = cv2.resize(gray, (1000, 800))
#cv2.imshow("gray", gray2)
gray = cv2.GaussianBlur(gray, GAUSS_KERNAL_SIZE, 1)
gray2 = cv2.resize(gray, (1000, 800))
#cv2.imshow("graygauss", gray2)
dst = cv2.Laplacian(gray, cv2.CV_64F, ksize=5)
abs_dst = cv2.convertScaleAbs(dst)
abs_dst = cv2.resize(abs_dst, (1000, 800))
#cv2.imshow("laplacian", abs_dst)
fm = variance_of_laplacian(gray)
text = "Not_Blurry"
key = cv2.waitKey(0)

# if the focus measure is less than the supplied threshold,
# then the image should be considered "blurry"
if fm < args["threshold"]:
    text = "Blurry"
    

# print out the result (replace the prints with whatever we want
# for the shell script to receive)
#print(args["image"] + ": " + text + " - Value: " + str(fm))
print(text)
# show the image
'''
cv2.putText(image, "{}: {:.2f}".format(text, fm), (10, 30),
cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 3)
cv2.imshow("Image", image)
key = cv2.waitKey(0)'''
