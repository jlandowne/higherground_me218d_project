# takePicandSend.sh
# This script is for the Pi0, and should be run on startup
#
# Description: Takes a picture using the arducam, sends the picture
# over to the central hub, and commands the central hub to examine the 
# photos, saving everything into files
# In a final version with a satpaq, this script can be modified to request
# the hub to send the data if the examine4Bugs.py script finds new bugs of 
# interest


# make sure we have waited long enough for the wifi to be on
sleep 5
# get the current time from the hub
datestr=`sshpass -p 'me218d' ssh pi@192.168.4.2 'date'`
# set the time on this pi
sudo date -s "$datestr"
# create a loop count variable
counter=3

/usr/bin/python3 /home/pi/Desktop/ME218D_Repo/higherground_me218d_project/Flash_LED_ON.py

# set an intial focus value
focus=1000

while [ $counter -gt 0 ]
do
	# change the focus value used in taking the picture
	focus=$( expr $focus - 100 )  
		
	# make sure the focus value isn't out of range
	if [ $focus -lt 0 ]
	then
		focus=0
	fi
	
	if [ $focus -gt 1000 ]; then
		response=1000
	fi 
	   
	
	#take a photo
	cd /home/pi/Documents/MIPI_Camera/RPI
	./capture -f $focus
	#sudo /home/pi/Documents/MIPI_Camera/RPI/capture -f $focus	
	sleep 5
	echo $counter #this just prints the counter 
	#send that file to the P4 (depending on who is running on the 0 side)
        #when you're testing, you're running out of pi 
	# but when you're running for real on startup, this is all run and 
	# saved in root, so the following code is done for each case
	# one of these pairs of commands will fail but that doens't matter

	#rename the file first
	sudo mv /home/pi/Documents/MIPI_Camera/RPI/mode3.jpg /home/pi/Pi0pic_new.jpg
	sudo mv /mode3.jpg /home/pi/Pi0pic_new.jpg
	# Ensure that the hub is on our nice list
	ssh-keyscan 192.168.4.2 >> ~/.ssh/known_hosts
	# send our picture over
	sshpass -p 'me218d' scp /home/pi/Pi0pic_new.jpg pi@192.168.4.2:	

	#sshpass -p 'me218d' scp /Pi0pic_new.jpg pi@192.168.4.2:

        #the pi4's response is to run python3 openCV stuff
	response=`sshpass -p 'me218d' ssh pi@192.168.4.2 'python3 blurinessTest.py -i Pi0pic_new.jpg'` 
	echo $response
	#decrement the counter
	counter=$( expr $counter - 1) 
	#if the response is good, just set the counter to 0 so we drop from the loop
	if [ "$response" == "Not_Blurry" ]; then
		counter=0
	fi


done

/usr/bin/python3 /home/pi/Desktop/ME218D_Repo/higherground_me218d_project/Flash_LED_OFF.py

#now perform the bug counting on the photos
sshpass -p 'me218d' ssh pi@192.168.4.2 'python3 examine4Bugs.py -f Pi0pic_old.jpg -s Pi0pic_new.jpg'

# replace the old photo with the new photo on the pi4
sshpass -p 'me218d' ssh pi@192.168.4.2 'sudo mv Pi0pic_new.jpg Pi0pic_old.jpg'
